﻿namespace fypnoodleshop4._0
{
    partial class Loginform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Loginform));
            this.lb_foodcourtservice = new System.Windows.Forms.Label();
            this.gb_credentials = new System.Windows.Forms.GroupBox();
            this.btn_submit = new System.Windows.Forms.Button();
            this.lb_password = new System.Windows.Forms.Label();
            this.lb_username = new System.Windows.Forms.Label();
            this.tb_password = new System.Windows.Forms.TextBox();
            this.tb_username = new System.Windows.Forms.TextBox();
            this.lb_sp = new System.Windows.Forms.Label();
            this.pb_spimg = new System.Windows.Forms.PictureBox();
            this.gb_credentials.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_spimg)).BeginInit();
            this.SuspendLayout();
            // 
            // lb_foodcourtservice
            // 
            this.lb_foodcourtservice.AutoSize = true;
            this.lb_foodcourtservice.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_foodcourtservice.Location = new System.Drawing.Point(231, 49);
            this.lb_foodcourtservice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_foodcourtservice.Name = "lb_foodcourtservice";
            this.lb_foodcourtservice.Size = new System.Drawing.Size(227, 30);
            this.lb_foodcourtservice.TabIndex = 0;
            this.lb_foodcourtservice.Text = "FoodCourt Service";
            // 
            // gb_credentials
            // 
            this.gb_credentials.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.gb_credentials.Controls.Add(this.btn_submit);
            this.gb_credentials.Controls.Add(this.lb_password);
            this.gb_credentials.Controls.Add(this.lb_username);
            this.gb_credentials.Controls.Add(this.tb_password);
            this.gb_credentials.Controls.Add(this.tb_username);
            this.gb_credentials.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gb_credentials.Location = new System.Drawing.Point(116, 148);
            this.gb_credentials.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gb_credentials.Name = "gb_credentials";
            this.gb_credentials.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.gb_credentials.Size = new System.Drawing.Size(436, 298);
            this.gb_credentials.TabIndex = 2;
            this.gb_credentials.TabStop = false;
            this.gb_credentials.Text = "Credentials ";
            // 
            // btn_submit
            // 
            this.btn_submit.Location = new System.Drawing.Point(153, 238);
            this.btn_submit.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btn_submit.Name = "btn_submit";
            this.btn_submit.Size = new System.Drawing.Size(134, 51);
            this.btn_submit.TabIndex = 4;
            this.btn_submit.Text = "Submit";
            this.btn_submit.UseVisualStyleBackColor = true;
            this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
            this.btn_submit.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btn_submit_KeyDown);
            // 
            // lb_password
            // 
            this.lb_password.AutoSize = true;
            this.lb_password.Location = new System.Drawing.Point(16, 178);
            this.lb_password.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_password.Name = "lb_password";
            this.lb_password.Size = new System.Drawing.Size(109, 25);
            this.lb_password.TabIndex = 3;
            this.lb_password.Text = "Password :";
            this.lb_password.Click += new System.EventHandler(this.label1_Click);
            // 
            // lb_username
            // 
            this.lb_username.AutoSize = true;
            this.lb_username.Location = new System.Drawing.Point(18, 75);
            this.lb_username.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lb_username.Name = "lb_username";
            this.lb_username.Size = new System.Drawing.Size(118, 25);
            this.lb_username.TabIndex = 2;
            this.lb_username.Text = "Username : ";
            // 
            // tb_password
            // 
            this.tb_password.Location = new System.Drawing.Point(153, 178);
            this.tb_password.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_password.Name = "tb_password";
            this.tb_password.Size = new System.Drawing.Size(214, 30);
            this.tb_password.TabIndex = 1;
            // 
            // tb_username
            // 
            this.tb_username.Location = new System.Drawing.Point(153, 75);
            this.tb_username.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.tb_username.Name = "tb_username";
            this.tb_username.Size = new System.Drawing.Size(214, 30);
            this.tb_username.TabIndex = 0;
            // 
            // lb_sp
            // 
            this.lb_sp.AutoSize = true;
            this.lb_sp.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_sp.ForeColor = System.Drawing.Color.Red;
            this.lb_sp.Location = new System.Drawing.Point(181, 49);
            this.lb_sp.Name = "lb_sp";
            this.lb_sp.Size = new System.Drawing.Size(49, 30);
            this.lb_sp.TabIndex = 4;
            this.lb_sp.Text = "SP";
            // 
            // pb_spimg
            // 
            this.pb_spimg.Image = ((System.Drawing.Image)(resources.GetObject("pb_spimg.Image")));
            this.pb_spimg.Location = new System.Drawing.Point(29, 95);
            this.pb_spimg.Name = "pb_spimg";
            this.pb_spimg.Size = new System.Drawing.Size(641, 364);
            this.pb_spimg.TabIndex = 3;
            this.pb_spimg.TabStop = false;
            // 
            // Loginform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(733, 575);
            this.Controls.Add(this.lb_sp);
            this.Controls.Add(this.gb_credentials);
            this.Controls.Add(this.lb_foodcourtservice);
            this.Controls.Add(this.pb_spimg);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Loginform";
            this.Text = "Foodcourt login";
            this.gb_credentials.ResumeLayout(false);
            this.gb_credentials.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_spimg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lb_foodcourtservice;
        private System.Windows.Forms.GroupBox gb_credentials;
        private System.Windows.Forms.Label lb_password;
        private System.Windows.Forms.Label lb_username;
        private System.Windows.Forms.TextBox tb_password;
        private System.Windows.Forms.TextBox tb_username;
        private System.Windows.Forms.Button btn_submit;
        private System.Windows.Forms.Label lb_sp;
        private System.Windows.Forms.PictureBox pb_spimg;
    }
}

