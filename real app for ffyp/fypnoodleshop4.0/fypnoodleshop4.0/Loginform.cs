﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace fypnoodleshop4._0
{
    public partial class Loginform : Form
    {
        public Loginform()
        {
            InitializeComponent();
            
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btn_submit_Click(object sender, EventArgs e)
        {
            login userdata = new login();// declaring new login object 
            userdata.Initialize(Convert.ToString(tb_username.Text), Convert.ToString(tb_password.Text)); // initialising userdata
            if (userdata.state == true)
            {
                MessageBox.Show("Correct credentials");
                Close();
            }
        }

        private void btn_submit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                btn_submit.PerformClick();
            }
        }
    }
}
