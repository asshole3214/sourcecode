﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace fypnoodleshop4._0
{
    class login
    {
        private string username { get; set; }// object username 
        private string password { get; set; } // object password 
        public bool state { get; set; } // whether the person credentials is correct or not 
        public login() // defualt case 
        {
            username = "";
            password = "";
        }

        public void Initialize(string user, string pass) // method for defualt data source 
        {

            username = user;    // settinng string username in gobal variable
            password = pass;    // settinng string passowrd in gobal variable
            if (username == "" || password == "")   // error handling 
            {
                MessageBox.Show(errormessage());    // calling method in errormessage(); 
            }
            state = referencefile(username, password, "userdatafile.txt");
            if (state == false) { MessageBox.Show(errormessage()); }

        }
        public void Initialize(string user, string pass, string reference)  // method for initialising if you want to refernce another data source 
        {

            username = user;    // settinng string username in gobal variable
            password = pass;    // settinng string passowrd in gobal variable
            if (username == "" || password == "")   // error handling 
            {
                MessageBox.Show(errormessage());    // calling method in errormessage(); 
            }
            state = referencefile(username, password, reference);
        }
        private bool referencefile(string user, string pass, string source) // method for referencing outside data 
                                                                            // this is working for txt file only (for now will update ltr ) 
        {
            StreamReader read = new StreamReader(source); // class to reference outside data
            string line = "";

            while (line != null) // checking if line is empty or not 
            {
                line = read.ReadLine(); // reading the line 
                string[] userdata = line.Split('\t'); // differeiate  the different datas 
                if (userdata[1] == user && userdata[2] == pass) // ensuring the data is valid
                {
                    //MessageBox.Show(userdata[1] + " "+userdata[2]);
                    return true;

                }
                if (read.EndOfStream) // if there is no match data 
                {
                    return false;
                }
            }
            read.Close();
            return false; // default return type 
        }
        private string errormessage() // method for error handli9ng 
        {
            return "Please enter a valid input";
        }
    }
}
