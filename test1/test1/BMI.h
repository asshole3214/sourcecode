#pragma once
#include <iostream>
#include <string>

using namespace std;

#ifndef BMI_H
#define BMI_h
class BMI 
{
public:
	//default constructors
	BMI();

	// overload constructor
	BMI(string , double, double);
	double getDoubleInput(string,double);

	//destructor
	~BMI();

	// access function 
	string getName() const;

private:
	//member variables 
	string newName;
	//string newQn;
	double newWeight;
	double newHeight;

};

#endif